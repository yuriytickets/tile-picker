module Tile
  module Scrapper
    class Factory
      def self.create vendor
        "Tile::Scrapper::Vendors::#{vendor.classify}".constantize.new
      rescue NameError
        "does not have #{vendor} scrapper"
      end
    end
  end
end
