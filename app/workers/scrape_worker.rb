require 'sidekiq/api'

class ScrapeWorker
  include Sidekiq::Worker
  include Sidetiq::Schedulable

  sidekiq_options queue: :scrape, retry: false, backtrace: false
  recurrence { daily.hour_of_day(0).minute_of_hour(10) }

  def perform
    vendors_to_scrape = YAML.load_file([Rails.root, 'config', 'tiles.yml'].join('/'))['vendors']
    vendors_to_scrape.each do |vendor|
      Tile::Manager.scrape_tiles_for vendor
    end
  end
end
