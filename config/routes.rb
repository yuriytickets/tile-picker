require 'sidekiq/web'

Rails.application.routes.draw do
  get 'choose_tiles' => 'tile#choose'
  mount Sidekiq::Web, at: "/sidekiq"
end
