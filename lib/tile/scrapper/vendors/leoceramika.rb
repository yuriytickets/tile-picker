module Tile
  module Scrapper
    module Vendors
      class Leoceramika
        VENDOR_NAME = 'leoceramika'
        SITE_URL = 'https://leoceramika.com/'
        TILES_MAIN_URI = 'katalog/keramichna-plytka'
        TILE_DIMENTIONS_REGEX = /(\d{1,3}(?:[\.\,]\d{1,2})?)\s*[x|X]\s*(\d{1,3}(?:[\.\,]\d{1,2})?)/
        TILES_DB_PATH = [Rails.root, 'db', 'tiles', "#{VENDOR_NAME}.yml"].join('/')
        DEFAULT_TILES_TO_SHOW = 3

        def scrape!
          tiles = scrape_items(final_links, "//*[@class='collection-price-images']/div", SITE_URL)
          File.open([Rails.root, 'db', 'tiles', 'leoceramika.yml'].join('/'), 'w') { |f| f.write tiles.to_yaml }
        end

        def get_best_tiles options
          tiles_from_db = YAML.load_file(TILES_DB_PATH)
          tiles_to_show = options[:tiles_to_show].present? ? options[:tiles_to_show].to_i : DEFAULT_TILES_TO_SHOW
          Tile::Calculator.tiles_with_bare_surface(tiles_from_db, options[:width], options[:height], tiles_to_show)
        end

        private

        def final_links
          tiles_all_url = SITE_URL + TILES_MAIN_URI
          tiles_all_doc = Nokogiri::HTML(open(tiles_all_url))
          country_links = tiles_all_doc.css('#column-wide a').map { |link| link['href'] }
          brand_links = scrape_links(country_links, '#column-wide a', SITE_URL)
          puts brand_links
          model_links = scrape_links(brand_links, 'ul.csc-menu li>a', SITE_URL)

          model_links
        end

        def scrape_links(urls, item_path, url_prefix = '')
          urls.map { |url|
            puts 'parsing' + url
            doc = Nokogiri::HTML(open("#{url_prefix}#{url}"))
            items = doc.css(item_path)
            items.map { |link| link['href'] }
          }.flatten
        end

        def scrape_items(urls, item_path, url_prefix = '')
          urls.map { |url|
            puts url
            doc = Nokogiri::HTML(open("#{url_prefix}#{url}"))
            items = doc.css(item_path)
            items = doc.xpath("//dd[@class='csc-textpic-caption']")
            puts items
            items.map { |item|
              item_name = item.text.strip
              item_dimentions = item.text.strip.match(TILE_DIMENTIONS_REGEX)
              item_height = item_dimentions.try(:captures).try(:[], 0)
              item_width = item_dimentions.try(:captures).try(:[], 1)
              next if (item_height.to_f * item_width.to_f).zero?

              measure_dimentions = Tile::Calculator.measure_dimentions(item_name, item_height.sub(',', '.').to_f, item_width.sub(',', '.').to_f)
              {
                name:   item_name,
                height: measure_dimentions[:height],
                width:  measure_dimentions[:width]
              }
            }
          }.flatten.compact
        end
      end
    end
  end
end
