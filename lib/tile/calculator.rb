module Tile
  class Calculator
    class << self
      def tiles_bare_surface(width, height, tile_width, tile_height)
        width = width * 100.0    # Converts width and height
        height = height * 100.0  # from m to cm
        square = width * height
        tile_covered_one = (width - (width%tile_width)) * (height - (height%tile_height)) # Calculating two possible variants
        tile_covered_two = (width - (width%tile_height)) * (height - (height%tile_width)) # of tile position (vertical or horizontal)

        return (square - [tile_covered_one, tile_covered_two].min)/100**2 # Uncovered in cm2
      end

      def tiles_with_bare_surface(tiles, width, height, tiles_to_show)
        tiles.each do |tile|
          tile[:bare_surface] = tiles_bare_surface(width, height, tile[:width], tile[:height])
        end

        tiles.sort_by { |t| t[:bare_surface] }.first(tiles_to_show)
      end

      def measure_dimentions(item_name, item_height, item_width)
        milimeters = item_name.match /(\d{1,3}(?:[\.\,]\d{1,2})?)\s*[x|X]\s*(\d{1,3}(?:[\.\,]\d{1,2})?).*(?:mm)/

        if milimeters
          { height: item_height.fdiv(10), width: item_width.fdiv(10) }
        else
          { height: item_height, width: item_width }
        end
      end
    end
  end
end
