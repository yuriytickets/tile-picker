class TileController < ApplicationController

  def choose
    @result = { tiles: {}, error: nil, params_example: 'vendor=leoceramika&width=20&height=3&tiles_to_show=50' }
    begin
      require_params(:vendor, :width, :height)
      vendor, width, height = params[:vendor], params[:width], params[:height]
      tiles_to_show = params[:tiles_to_show]
      @tiles = Tile::Manager.select_tiles_for vendor, { width: width.to_f, height: height.to_f, tiles_to_show: tiles_to_show }
    rescue ActionController::ParameterMissing => exc
      @result[:error] = exc.message # rescued separetaly for ability to update behavior
    rescue => exc
      @result[:error] = exc.message
    end
    render "tiles/choosen"
  end
end
