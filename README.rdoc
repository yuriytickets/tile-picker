== README

Tiles picker

- Scrapes available tiles from sites using Nokokiri
- Chooses best tiles for covering rectangular surface

For scraping uses worker that starts in given reccurence (Sidekiq)
To choose tiles use GET request as in example
Example: 
  http://127.0.0.1:3000/choose_tiles?vendor=leoceramika&width=20&height=3.33&tiles_to_show=50
  required params: 
    - vendor
    - width
    - height
  optional params:
    - tiles_to_show (if not given, selects default num of tiles (3))