class ApplicationController < ActionController::API

  def require_params *req_params
    req_params.each_with_object(params) do |key, obj|
      obj.require(key)
    end
  end
end
