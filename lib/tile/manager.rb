module Tile
  class Manager
    class << self
      def select_tiles_for vendor, options = {}
        Tile::Scrapper::Factory.create(vendor).get_best_tiles(options)
      end

      def scrape_tiles_for vendor
        Tile::Scrapper::Factory.create(vendor).scrape!
      end
    end
  end
end
